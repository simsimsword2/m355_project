import java.util.Date;

public class Person {

    private String name;
    private String firstname;
    private Date birthdate;
    private int age;
    /** 0 = female and 1 = male **/
    private Boolean gender;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public Date getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(Date birthdate) {
        this.birthdate = birthdate;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Boolean getGender() {
        return gender;
    }

    public void setGender(Boolean gender) {
        this.gender = gender;
    }


    public Person(String name, String firstname, Date birthdate, int age, Boolean gender) {
        this.name = name;
        this.firstname = firstname;
        this.birthdate = birthdate;
        this.age = age;
        this.gender = gender;
    }
}
